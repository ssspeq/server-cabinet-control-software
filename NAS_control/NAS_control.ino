//I/O EXPANDER
#include "Adafruit_MCP23017.h"
Adafruit_MCP23017 mcp;
//DISPLAY-------------------------------------------------------------------------
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <SPI.h>
#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
#define OLED_DC     48
#define OLED_CS     47
#define OLED_RESET  -1
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT,
                         &SPI, OLED_DC, OLED_RESET, OLED_CS);
int displayScreen = 0;
const int clockScreen = 0;
const int mainMenuScreen = 1;
const int pidSettings = 2;
int currentMenuItem = 0;
static const unsigned long DISPLAY_RESET_INTERVAL = 10000; // ms
unsigned long lastRefreshTimeDISPLAY = 0;


//MAGNET SWITCH TEST
#define MAGNETSWITCH  24

//ALARM BUZZER-------------------------------------------------------------------
#define PIEZOPIN 4
bool lastState = false;
bool soundAlarm = false;
bool buzzerActive = false;
bool alarmForced = false;


//INFRARED------------------------------------------------------------------------
#include <IRremote.h>
#define RECV_PIN 12
IRrecv irrecv(RECV_PIN);
decode_results results;
double lastCode = 0;
static unsigned long lastButtonPressedTime = 0;


//buttons-------------------------------------------------------------------------
const int buttonUp = 0;
const int buttonDown = 1;
const int buttonLeft = 2;
const int buttonRight = 3;
const int buttonCircle = 4;
const int buttonA = 5;
const int buttonB = 6;
const int buttonC = 7;
const int buttonPower = 8;

//RTC-----------------------------------------------------------------------------
#include <Wire.h>
#include <RTClib.h>
RTC_DS1307 rtc = RTC_DS1307();
int hours = 0;
int minutes = 0;
int seconds = 0;
bool blinkColon = false;

//DIM------------------------------------------------------------------------------
int currentBrightness = 0;

//FAN
#define PWMPIN 5
#define FANPIN 8
#define RPMPIN 3
unsigned int rpm = 0;
volatile unsigned long pulse = 0;
static unsigned long REFRESH_INTERVAL_RPM = 1000; // ms
unsigned long lastRefreshTimeRPM = 0;
unsigned int STABLE_TIME = 330;
volatile unsigned long previousTime;

//TEMPERATURE----------------------------------------------------------------------
#include <DHT.h>
#define DHTTYPE DHT11
#define DHTPIN 45
DHT dht(DHTPIN, DHTTYPE);
static unsigned long REFRESH_INTERVAL_TEMP = 10000; // ms
unsigned long lastRefreshTimeTemp = 0;

//PID------------------------------------------------------------------------------
#include <PID_v1.h>
#include <EEPROMex.h>

int setpointAddress = EEPROM.getAddress(sizeof(double));
int KpAddress = EEPROM.getAddress(sizeof(double));
int KiAddress = EEPROM.getAddress(sizeof(double));
int KdAddress = EEPROM.getAddress(sizeof(double));

double Setpoint;
//double Kp;
//double Ki;
//double Kd;
double Input;
double Output;

double Kp = 7, Ki = 0.5, Kd = 1;

PID myPID(&Input, &Output, &Setpoint, Kp, Ki, Kd, REVERSE);

//JSON------------------------------------------------------------------------------
#include <ArduinoJson.h>
String inData = "";
String command = "";



void setup() {
  //CHANGE PWM FREQUENCY
  //TCCR3B = TCCR3B & B11111000 | B00000001;
  TCCR4A = 0;
  TCCR4B = 0;
  TCNT4  = 0;

  // Mode 10: phase correct PWM with ICR4 as Top (= F_CPU/2/25000)
  // OC4C as Non-Inverted PWM output
  ICR4   = (F_CPU / 25000) / 2;
  OCR4C  = ICR4 / 2;                  // default: about 50:50
  TCCR4A = _BV(COM4C1) | _BV(WGM41);
  TCCR4B = _BV(WGM43) | _BV(CS40);
  pinMode( 8, OUTPUT);


  Serial.begin(115200);
  delay(100);
  //Set sane inital values to EEPROM
  //EEPROM.setMaxAllowedWrites(50);
  //EEPROM.updateDouble(setpointAddress, 28);
  //EEPROM.updateDouble(KpAddress, 4);
  //EEPROM.updateDouble(KiAddress, 0.5);
  //EEPROM.updateDouble(KdAddress, 0.5);

  //Read PID values from EEPROM
  Setpoint = EEPROM.readDouble(setpointAddress);
  Kp = EEPROM.readDouble(KpAddress);
  Ki = EEPROM.readDouble(KiAddress);
  Kd = EEPROM.readDouble(KdAddress);

  mcp.begin();      // use default address 0
  mcp.pinMode(0, OUTPUT);
  mcp.pinMode(1, OUTPUT);
  pinMode(RPMPIN, INPUT_PULLUP);
  pinMode(MAGNETSWITCH, INPUT_PULLUP);

  attachInterrupt(digitalPinToInterrupt(RPMPIN), countPulse, CHANGE);

  //DISPLAY
  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if (!display.begin(SSD1306_SWITCHCAPVCC)) {
    Serial.println(F("DEBUG:SSD1306 allocation failed"));
    for (;;); // Don't proceed, loop forever
  }
  display.clearDisplay();
  // Setup function runs once at startup to initialize the display
  // and DS1307 clock.
  dht.begin();
  // Setup Serial port to print debug output.
  irrecv.enableIRIn(); // Start the receiver
  // Setup the DS1307 real-time clock.
  rtc.begin();

  // Set the DS1307 clock if it hasn't been set before.
  bool setClockTime = !rtc.isrunning();
  // Alternatively you can force the clock to be set again by
  // uncommenting this line:
  //setClockTime = true;
  if (setClockTime) {
    Serial.println("DEBUG:Setting DS1307 time!");
    // This line sets the DS1307 time to the exact date and time the
    // sketch was compiled:
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // Alternatively you can set the RTC with an explicit date & time,
    // for example to set January 21, 2014 at 3am you would uncomment:
    //rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }
  //initialize the variables we're linked to
  dht.readTemperature();

  //turn the PID on
  myPID.SetMode(AUTOMATIC);
}

void loop() {

  if (irrecv.decode(&results)) {
    lastButtonPressedTime = millis();
    //Serial.println(results.value, HEX);

    if (results.value == 0x10EF00FF) {
      Serial.println("REMOTE:Down");
      lastCode = buttonDown;
    }
    if (results.value == 0x10EFA05F) {
      Serial.println("REMOTE:Up");
      lastCode = buttonUp;
    }
    if (results.value == 0x10EF20DF) {
      Serial.println("REMOTE:Circle");
      lastCode = buttonCircle;
    }
    if (results.value == 0x10EF10EF) {
      Serial.println("REMOTE:Left");
      lastCode = buttonLeft;
    }
    if (results.value == 0x10EF807F) {
      Serial.println("REMOTE:Right");
      lastCode = buttonRight;
    }
    if (results.value == 0x10EFF807) {
      Serial.println("REMOTE:A");
      lastCode = buttonA;
    }
    if (results.value == 0x10EF7887) {
      Serial.println("REMOTE:B");
      lastCode = buttonB;
    }
    if (results.value == 0x10EF58A7) {
      Serial.println("REMOTE:C");
      lastCode = buttonC;
    }
    if (results.value == 0x10EFD827) {
      Serial.println("REMOTE:Power");
      lastCode = buttonPower;
    }
    if (results.value == 0xFFFFFFFF) {
      Serial.println("REMOTE:repeat code");
    }
    //Button logic
    if (lastCode == buttonA) {
      Serial.println("DEBUG:go to PID settings");
      displayScreen = 2;
    }
    irrecv.resume(); // Receive the next value
  }

  if (Serial.available() > 0)
  {
    handleCommand();
  }


  if (millis() - lastButtonPressedTime >= DISPLAY_RESET_INTERVAL)
  {
    displayScreen = 0;
  }
  maintainDisplay();
  maintainTemperatureSensor();
  //checkFanPin();
  calculateRPM();
  myPID.Compute();
  //analogWrite(FANPIN, Output);
  analogWrite25k(Output);
  alarm();
}

void analogWrite25k(int value)
{
  OCR4C = value;
}

void updatePIDTuningParameters() {
  myPID.SetTunings(Kp, Ki, Kd);
  EEPROM.updateDouble(setpointAddress, Setpoint);
  EEPROM.updateDouble(KpAddress, Kp);
  EEPROM.updateDouble(KiAddress, Ki);
  EEPROM.updateDouble(KdAddress, Kd);
}

void handleCommand() {
  inData = Serial.readStringUntil('\n');
  DynamicJsonDocument doc(300);
  DeserializationError error = deserializeJson(doc, inData);
  if (error) {
    Serial.println("ERROR:JSON error");
    return;
  }
  command = doc["command"].as<String>();
  if (doc["command"].as<String>() == "get") {
    //We need to return a value, check which one
    if (doc["variable"].as<String>() == "variables") {
      StaticJsonDocument<1024> doc;
      if (alarmForced) {
        doc["alarm"] = "on";
      }
      else {
        doc["alarm"] = "off";
      }
      doc["temp"] = Input;
      doc["fan_rpm"] = rpm;
      doc["setpoint"] = Setpoint;
      doc["pid_output"] = Output;
      doc["Kp"] = Kp;
      doc["Ki"] = Ki;
      doc["Kd"] = Kd;
      if (digitalRead(MAGNETSWITCH)) {
        doc["door"] = "open";
      }
      else {
        doc["door"] = "closed";
      }
      serializeJson(doc, Serial);
      Serial.print("\n");
    }
  }
  if (doc["command"].as<String>() == "set") {
    //We need to set a value, check which one
    if (doc["variable"].as<String>() == "Kp") {
      Kp = doc["value"];
      updatePIDTuningParameters();
    }
    if (doc["variable"].as<String>() == "Ki") {
      Ki = doc["value"];
      updatePIDTuningParameters();
    }
    if (doc["variable"].as<String>() == "Kd") {
      Kd = doc["value"];
      updatePIDTuningParameters();
    }
    if (doc["variable"].as<String>() == "setpoint") {
      Setpoint = doc["value"];
      updatePIDTuningParameters();
    }
    if (doc["variable"].as<String>() == "alarm") {
      if (doc["value"].as<String>() == "on") {
        alarmForced = true;
      }
      else {
        alarmForced = false;
      }
    }

  }
}

void drawPIDMenu() {
  display.clearDisplay();
  display.setTextSize(1);             // Normal 1:1 pixel scale
  display.setTextColor(WHITE);        // Draw white text
  display.cp437(true);
  display.setCursor(0, 0);            // Start at top-left corner
  display.println("PID SETTINGS");

  display.drawLine(0, 8, display.width(), 8, WHITE);
  display.drawLine(0, 9, display.width(), 9, BLACK);

  display.println("Kp:");
  display.setTextSize(2);
  if (currentMenuItem == 0) {
    display.setTextColor(BLACK, WHITE);
  }
  else {
    display.setTextColor(WHITE);
  }
  display.println(Kp);

  display.setTextColor(WHITE);
  display.setTextSize(1);
  display.println("Ki:");
  if (currentMenuItem == 1) {
    display.setTextColor(BLACK, WHITE);
  }
  else {
    display.setTextColor(WHITE);
  }
  display.setTextSize(2);
  display.println(Ki);

  display.setTextColor(WHITE);
  display.setCursor(64, 9);
  display.setTextSize(1);
  display.print("Kd:");
  if (currentMenuItem == 2) {
    display.setTextColor(BLACK, WHITE);
  }
  else {
    display.setTextColor(WHITE);
  }
  display.setCursor(64, 16);
  display.setTextSize(2);
  display.print(Kd);

  display.setTextColor(WHITE);
  display.setCursor(64, 32);
  display.setTextSize(1);
  display.println("Sp:");
  if (currentMenuItem == 3) {
    display.setTextColor(BLACK, WHITE);
  }
  else {
    display.setTextColor(WHITE);
  }
  display.setCursor(64, 40);
  display.setTextSize(2);
  display.println(Setpoint);
  display.drawLine(0, 56, display.width(), 56, WHITE);

  display.setCursor(64, 57);
  display.setTextSize(1);
  if (currentMenuItem == 4) {
    display.setTextColor(BLACK, WHITE);
  }
  else {
    display.setTextColor(WHITE);
  }
  display.println("BACK:");
  display.display();
  if (lastCode == buttonDown and currentMenuItem <= 4) {
    currentMenuItem++;
  }
  if (lastCode == buttonUp and currentMenuItem >= 0) {
    currentMenuItem--;
  }
  if (currentMenuItem < 0) {
    currentMenuItem = 4;
  };
  if (currentMenuItem > 4) {
    currentMenuItem = 0;
  };
  if (lastCode == buttonRight and currentMenuItem == 0) {
    Kp = Kp + 0.1;
  }
  if (lastCode == buttonLeft and currentMenuItem == 0) {
    Kp = Kp - 0.1;
  }
  if (lastCode == buttonRight and currentMenuItem == 1) {
    Ki = Ki + 0.1;
  }
  if (lastCode == buttonLeft and currentMenuItem == 1) {
    Ki = Ki - 0.1;
  }
  if (lastCode == buttonRight and currentMenuItem == 2) {
    Kd = Kd + 0.1;
  }
  if (lastCode == buttonLeft and currentMenuItem == 2) {
    Kd = Kd - 0.1;
  }
  if (lastCode == buttonRight and currentMenuItem == 3) {
    Setpoint++;
  }
  if (lastCode == buttonLeft and currentMenuItem == 3) {
    Setpoint--;
  }
  if (lastCode == buttonCircle and currentMenuItem == 4) {
    updatePIDTuningParameters();
    displayScreen = 0;
  }
  lastCode = 256;
}

void drawInfo() {
  static const unsigned long REFRESH_INTERVAL = 1000; // ms
  static unsigned long lastRefreshTime = 0;

  if (millis() - lastRefreshTime >= REFRESH_INTERVAL)
  {
    lastRefreshTime += REFRESH_INTERVAL;
    DateTime now = rtc.now();
    // Print out the time for debug purposes:
    /*Serial.print("Read date & time from DS1307: ");
      Serial.print(now.year(), DEC);
      Serial.print('/');
      Serial.print(now.month(), DEC);
      Serial.print('/');
      Serial.print(now.day(), DEC);
      Serial.print(' ');
      Serial.print(now.hour(), DEC);
      Serial.print(':');
      Serial.print(now.minute(), DEC);
      Serial.print(':');
      Serial.print(now.second(), DEC);
      Serial.println();
      // Now set the hours and minutes.*/
    hours = now.hour();
    minutes = now.minute();
    int x = random(1, 68);
    int y = random(1, 45);
    display.clearDisplay();
    display.setTextSize(2);             // Normal 1:1 pixel scale
    display.setTextColor(WHITE);        // Draw white text
    display.cp437(true);
    display.setCursor(x, y);            // Start at top-left corner
    display.print(hours, DEC); display.print(":"); display.print(minutes, DEC);
    display.setCursor(x, y + 16);
    display.print(Input);
    display.display();
    //Serial.println(digitalRead(MAGNETSWITCH));
  }
}

void lowerBrightness() {
  Serial.println("DEBUG:in lowered");
  if (currentBrightness > 0) {
    currentBrightness -= 1;
    //    clockDisplay.setBrightness(currentBrightness);
    //    matrix.setBrightness(currentBrightness);
    Serial.println("DEBUG:lowered");
  }
}

void raiseBrightness() {
  Serial.println("DEBUG:in raised");
  if (currentBrightness < 16) {
    currentBrightness += 1;
    //    clockDisplay.setBrightness(currentBrightness);
    //    matrix.setBrightness(currentBrightness);
    Serial.println("DEBUG:raised");
  }
}
void maintainDisplay()
{

  static const unsigned long REFRESH_INTERVAL = 100; // ms
  static unsigned long lastRefreshTime = 0;

  if (millis() - lastRefreshTime >= REFRESH_INTERVAL)
  {
    lastRefreshTime += REFRESH_INTERVAL;
    //CHeck what screen should be displayed
    if (displayScreen == 0) {
      drawInfo();
    }
    if (displayScreen == 2) {
      drawPIDMenu();
    }
    if (Input > Setpoint) {
      mcp.digitalWrite(0, LOW);
      mcp.digitalWrite(1, HIGH);
    }
    else {
      mcp.digitalWrite(0, HIGH);
      mcp.digitalWrite(1, LOW);

    }
  }
}

/*void checkFanPin()
  {
  static const unsigned long REFRESH_INTERVAL = 1; // ms
  static unsigned long lastRefreshTime = 0;

  if (millis() - lastRefreshTime >= REFRESH_INTERVAL)
  {
    lastRefreshTime += REFRESH_INTERVAL;
    if (lastState != digitalRead(RPMPIN)) {
      lastState = digitalRead(RPMPIN);
      countPulse();
    }
  }
  }
*/
void calculateRPM()
{
  if (millis() > lastRefreshTimeRPM + REFRESH_INTERVAL_RPM)
  {
    long elapsed = millis() - lastRefreshTimeRPM;
    long pulses;
    lastRefreshTimeRPM = millis();
    noInterrupts();
    pulses = pulse;
    pulse = 0;
    interrupts();
    rpm = pulses / 4*60;

    Serial.println("DEBUG: pulses" + String(pulses));
    //Serial.println("DEBUG: elapsed"+String(elapsed));
  }
}
void countPulse()
{
  unsigned long currentTime = micros();
  if (currentTime - previousTime >= STABLE_TIME) pulse++;
  previousTime = currentTime;
}

void alarm()
{
  static const unsigned long REFRESH_INTERVAL = 100; // ms
  static unsigned long lastRefreshTime = 0;

  if (millis() - lastRefreshTime >= REFRESH_INTERVAL)
  {
    lastRefreshTime += REFRESH_INTERVAL;
    //Sound alarm if temp too high or fan has stopped
    if (Input > 35 or rpm < 10) {
      soundAlarm = false;
    }
    else {
      soundAlarm = false;
    }
    if (buzzerActive and soundAlarm) {
      analogWrite(PIEZOPIN, 0);
      buzzerActive = false;
    }
    else if (!buzzerActive and soundAlarm or alarmForced) {
      analogWrite(PIEZOPIN, 220);
      buzzerActive = true;
    }
    else {
      analogWrite(PIEZOPIN, 0);
      buzzerActive = false;
    }
  }
}

void maintainTemperatureSensor()
{
  if (millis() > lastRefreshTimeTemp + REFRESH_INTERVAL_TEMP)
  {
    lastRefreshTimeTemp = millis();
    readTemp();
  }
}

void readTemp()
{
  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  //float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  //float f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(t)) {
    Serial.println(F("ERROR:Failed to read from DHT sensor!"));
    return;
  }

  // Compute heat index in Fahrenheit (the default)
  //float hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
  //float hic = dht.computeHeatIndex(t, h, false);

  /*Serial.print(F("Humidity: "));
    Serial.print(h);
    Serial.print(F("%  Temperature: "));
    Serial.print(t);
    Serial.print(F("°C "));
    Serial.print(f);
    Serial.print(F("°F  Heat index: "));
    Serial.print(hic);
    Serial.print(F("°C "));
    Serial.print(hif);
    Serial.println(F("°F"));*/
  //Input = (double) t - 3;
  Input = t - 3;
}

void refreshDisplay() {
  display.clearDisplay();

  display.setTextSize(1);             // Normal 1:1 pixel scale
  display.setTextColor(WHITE);        // Draw white text
  display.setCursor(0, 0);            // Start at top-left corner
  display.println(F("Hello, world!"));

  display.setTextColor(BLACK, WHITE); // Draw 'inverse' text
  display.println(3.141592);

  display.setTextSize(2);             // Draw 2X-scale text
  display.setTextColor(WHITE);
  display.print(F("0x")); display.println(0xDEADBEEF, HEX);

  display.display();
  if (minutes == 0) {
    // Get the time from the DS1307.
    //    DateTime now = rtc.now();
    // Print out the time for debug purposes:
    Serial.print("Read date & time from DS1307: ");
    //    Serial.print(now.year(), DEC);
    Serial.print('/');
    //    Serial.print(now.month(), DEC);
    Serial.print('/');
    //    Serial.print(now.day(), DEC);
    Serial.print(' ');
    //    Serial.print(now.hour(), DEC);
    Serial.print(':');
    //    Serial.print(now.minute(), DEC);
    Serial.print(':');
    //    Serial.print(now.second(), DEC);
    Serial.println();
    // Now set the hours and minutes.
    //    hours = now.hour();
    //    minutes = now.minute();

  }

  // Show the time on the display by turning it into a numeric
  // value, like 3:30 turns into 330, by multiplying the hour by
  // 100 and then adding the minutes.
  int displayValue = hours * 100 + minutes;
  // Now print the time value to the display.
  //  clockDisplay.print(displayValue, DEC);

  // Add zero padding when in 24 hour mode and it's midnight.
  // In this case the print function above won't have leading 0's
  // which can look confusing.  Go in and explicitly add these zeros.
  if (hours == 0) {
    // Pad hour 0.
    //    clockDisplay.writeDigitNum(1, 0);
    // Also pad when the 10's minute is 0 and should be padded.
    if (minutes < 10) {
      //      clockDisplay.writeDigitNum(2, 0);
    }
  }

  // Blink the colon by flipping its value every loop iteration
  // (which happens every second).
  blinkColon = !blinkColon;
  //  clockDisplay.drawColon(blinkColon);

  // Now push out to the display the new values that were set above.
  //  clockDisplay.writeDisplay();

  // Now increase the seconds by one.
  seconds += 1;
  // If the seconds go above 59 then the minutes should increase and
  // the seconds should wrap back to 0.
  if (seconds > 59) {
    seconds = 0;
    minutes += 1;
    // Again if the minutes go above 59 then the hour should increase and
    // the minutes should wrap back to 0.
    if (minutes > 59) {
      minutes = 0;
      hours += 1;
      // Note that when the minutes are 0 (i.e. it's the top of a new hour)
      // then the start of the loop will read the actual time from the DS1307
      // again.  Just to be safe though we'll also increment the hour and wrap
      // back to 0 if it goes above 23 (i.e. past midnight).
      if (hours > 23) {
        hours = 0;
      }
    }
  }
}
